package ru.vmaksimenkov.tm;

import ru.vmaksimenkov.tm.constant.TerminalConst;

public class Application {

    public static void main(final String[] args) {
        displayWelcome();
        run(args);
    }

    private static void displayHelp() {
        System.out.println(TerminalConst.CMD_VERSION + " - Display program version.");
        System.out.println(TerminalConst.CMD_ABOUT + " - Display developer info.");
        System.out.println(TerminalConst.CMD_HELP + " - Display list of terminal commands.");
        System.exit(0);
    }

    private static void displayVersion() {
        System.out.println("1.0.0");
        System.exit(0);
    }

    private static void displayAbout() {
        System.out.println("Vlad Maximenkov");
        System.out.println("vmaksimenkov@tsconsulting.com");
        System.exit(0);
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void run(final String[] args) {
        if (args == null || args.length < 1) return;
        if (TerminalConst.CMD_HELP.equals(args[0])) displayHelp();
        if (TerminalConst.CMD_VERSION.equals(args[0])) displayVersion();
        if (TerminalConst.CMD_ABOUT.equals(args[0])) displayAbout();
    }
}
