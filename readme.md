# Screenshots

https://yadi.sk/d/yI5zu0TyBG8vzg

# Task Manager

Console application, being developed during Java learning course

## Authors

* **Vladislav Maximenkov** - *vmaksimenkov@tsconsulting.com*

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### System requirements

Make sure that JDK (ver. 15.0.1 or above) is installed, than add its /bin/ folder path into environment variable PATH

Tested on:
* Windows 10
* 16GB RAM
* i7 CPU

### Running program

Compile project, than run:

```
java -jar task-manager.jar
```

Type help to see available commands:

```
java -jar task-manager.jar help
```

## Built With

* [IntelliJ IDEA 2020.03 Community Edition](https://www.jetbrains.com/idea/) - The IDE used
